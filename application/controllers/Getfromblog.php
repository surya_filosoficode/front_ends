<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Getfromblog extends CI_Controller {

	public function __construct(){
        parent::__construct(); 
        date_default_timezone_set("Asia/Bangkok");
        $this->load->model("main/mainmodel", "mm");

        $this->load->model("main/mainmodel_blog", "mb");
    }

	public function index(){
		// $this->load->view("main_page/home_page");
		$db2 = $this->load->database('db_blog', TRUE);
		$data = $this->mb->get_data_all_where("wp_posts", array("ID"=>"6"));

		// print_r("<pre>");
		print_r($data[0]->post_title);
		print_r($data[0]->post_content);
	}
}
