<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Homepage extends CI_Controller {

	public function __construct(){
        parent::__construct(); 
        
        $this->load->model('main/mainmodel', 'mm');
        // $this->load->model('main/main', 'mm');
        $this->load->model('promote/main_promote', 'mp');
        $this->load->model('homepage/main_homepage', 'mh');

        $this->load->library("response_message");
        $this->load->library("Auth_v0");
        $this->load->library("magic_pattern");
        $this->load->library('Perpage');
        
        $this->load->library("Time_master");

        date_default_timezone_set("Asia/Bangkok");
    }

	public function index(){
		$data["list_article"] = $this->mm->get_data_all_where("article_main", ["is_delete_article"=>"0","status_check_article"=>"1","status_post_article"=>"1"]);
        $data["list_pr_article"] = $this->mp->get_promote_all([]);

        $promote_pr = $this->mp->get_promote_pg_home_all(["sts_pr"=>"1","active_pr"=>"1", "r_num_pr !="=>"0"]);

        $list_pr_product = [];

        foreach ($promote_pr as $key => $value) {
        	$id_jenis = $value->id_jenis;
        	$align_header_pr = $value->align_header_pr;
        	$r_num_pr = $value->r_num_pr;

        	$tgl_add_pr = $value->tgl_add_pr;
        	$add_by_pr = $value->add_by_pr;

        	$sts_pr = $value->sts_pr;
        	$active_pr = $value->active_pr;

        	$nama_jenis = strtolower($value->nama_jenis);
        	$parent_id = $value->parent_id;
        	// print_r("<pre>");
        	// print_r($nama_jenis);

        	$product_data = $this->mh->get_product_for_home_page([], $nama_jenis, 6, 1);

        	$arr_tmp = ["category"=>$value,
        				"product"=>$product_data
        			];

        	if($product_data){
        		array_push($list_pr_product, $arr_tmp);
        	}
        }

        $data["list_pr_product"] = $list_pr_product;

        // print_r("<pre>");
        // print_r($data["list_pr_product"]);
        // die();

        // print_r("<pre>");
        // print_r($data["list_pr_product"]);
        // print_r($promote_pr);
		$this->load->view("main_page/home_page", $data);
	}

	public function blog(){
		$data["list_article"] = [];
		if(isset($_GET["id_ses"])){
			if($_GET["id_ses"]){
				$id_ses = $_GET["id_ses"];
				$data["list_article"] = $this->mm->get_data_each("article_main", ["sha2(id_article_main, '256') = "=>$id_ses, "is_delete_article"=>"0"]);
			}
		}
		
		$this->load->view("main_page/static_page", $data);
	}

    public function blog_all(){
        $data["list_category"]  = $this->mm->get_data_all_where("article_jenis", []);
        $data["article_tipe"]  = $this->mm->get_data_all_where("article_tipe", []);
        $data["list_article"]   = $this->mp->get_promote_all_blog(["is_delete_article"=>"0", "status_post_article"=>"1"]);
        
        if(isset($_GET["id_ses"])){
            if($_GET["id_ses"]){
                $id_ses = $_GET["id_ses"];
                $data["list_article"] = $this->mp->get_promote_all_blog(["tipe_article"=>$id_ses, "is_delete_article"=>"0", "status_post_article"=>"1"]);
            }
        }
        // print_r("<pre>");
        // print_r($data);
        $this->load->view("main_page/blog_page", $data);
    }

	public function about_us(){
		$this->load->view("main_page/about_us");
	}

	public function contact(){
		$this->load->view("main_page/contact");
	}

	public function product_ex(){
        $promote_pr = $this->mp->get_promote_pg_home_all(["active_pr"=>"1", "r_num_pr !="=>"0"]);
        $data["list_pr_category"] = $promote_pr;
        $data["str_header"] = "Seluruh Produk";

        if(isset($_GET["cate"])){
            if($_GET["cate"]){

                if($_GET["cate"] == "all")
                    $promote_pr = $this->mp->get_promote_pg_home_all(["active_pr"=>"1", "r_num_pr !="=>"0"]);
                else
                    $promote_pr = $this->mp->get_promote_pg_home_all(["active_pr"=>"1", "r_num_pr !="=>"0", "sha2(pph.id_jenis, '256') = "=>$_GET["cate"]]);
            }
        }
		

        $list_pr_product = [];
        $list_pr_category = [];

        foreach ($promote_pr as $key => $value) {
        	$id_jenis = $value->id_jenis;
        	$align_header_pr = $value->align_header_pr;
        	$r_num_pr = $value->r_num_pr;

        	$tgl_add_pr = $value->tgl_add_pr;
        	$add_by_pr = $value->add_by_pr;

        	$sts_pr = $value->sts_pr;
        	$active_pr = $value->active_pr;

        	$nama_jenis = strtolower($value->nama_jenis);
        	$parent_id = $value->parent_id;
        	// print_r("<pre>");
        	// print_r($nama_jenis);

        	$product_data = $this->mp->get_product_all_like([], $nama_jenis);

        	$arr_tmp = ["category"=>$value,
        				"product"=>$product_data
        			];

        	if($product_data){
        		array_push($list_pr_product, $arr_tmp);
        		// array_push($list_pr_category, $value);
        	}
            
        }

        if(isset($_GET["cate"])){
            if($_GET["cate"]){
                $data["str_header"] = "Produk ".$value->nama_jenis;
            }
        }

        $data["list_pr_product"] = $list_pr_product;
        

        // print_r("<pre>");
        // print_r($data);

		$this->load->view("main_page/product", $data);
	}

    public function product(){
        $promote_pr = $this->mp->get_promote_pg_home_all(["active_pr"=>"1", "r_num_pr !="=>"0"]);
        $data["list_pr_category"] = $promote_pr;
        $data["str_header"] = "Seluruh Produk";

        $paginationlink = "getresult.php?page=";
        $per_page = 10;
        if(!empty($_GET["page"])) {
            $per_page = (int) $_GET["page"];
        }

        $start = 1;
        if(!empty($_GET["start"])) {
            $start = (int) $_GET["start"];
        }

        $limit = ($start-1)*$per_page;
        if($limit < 0) $limit = 0;

        $data["output"] = $this->perpage->getAllPageLinks(78, $paginationlink, (int)$per_page);

        $product_data = [];
        if(isset($_GET["cate"])){
            if($_GET["cate"]){

                if($_GET["cate"] == "all"){
                    $this->db->order_by("id_product", "DESC");
                    $product_data = $this->mm->get_data_all_where("product", []);
                } else{
                    $get_jenis = $this->mm->get_data_each("product_jenis", ["sha2(id_jenis, '256') = "=>$_GET["cate"]]);

                    $nama_jenis = $get_jenis["nama_jenis"];
                    

                    $this->db->order_by("id_product", "DESC");
                    $product_data = $this->mp->get_product_all_like([], $nama_jenis);
                }
            }
        }else {
            $this->db->order_by("id_product", "DESC");
            $product_data = $this->mm->get_data_all_where("product", []);
        }

        $this->mh->get_product_for_home_page_all([], $config['per_page'], $from);

        // print_r("<pre>");
        // print_r($product_data);
        // die();

        $data["list_pr_product"] = $product_data;

        $this->load->view("main_page/product", $data);
    }


    public function product_detail(){
        $data["product_detail"] = [];

        if(isset($_GET["prd"])){
            if($_GET["prd"]){
                $data["product_detail"] = $this->mp->get_product_detail(["sha2(pr.id_product, '256') = "=>$_GET["prd"]]);
            }
        }

        // print_r("<pre>");
        // print_r($data);

        $this->load->view("main_page/product_detail", $data);
    }


    public function val_form(){
        $config_val_input = array(
                array(
                    'field'=>'inp',
                    'label'=>'Isian',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                )
            );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function get_answer(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "inp"=>""
                );
        
        $def_return_arr = ["title_inp"=>"Tell me who am i, my nickname is..?", "def_num"=>"19", "data"=>"0"];
        if($this->val_form()){
            $inp = strtolower($this->input->post('inp'));
            $def_num = $this->input->post('def_num');

            if($def_num == "19" && $inp == "ara"){
                $def_return_arr = ["title_inp"=>"Who you are ..? *special name", "def_num"=>"13", "data"=>"0"];
                $msg_main = array("status"=>true, "msg"=>"Good you alright.. :), Next Question ya..");
            }elseif ($def_num == "13" && $inp == "adek sugar") {
                $def_return_arr = ["title_inp"=>"", "def_num"=>"", "data"=>"Halo Adek Sugar.<br> Lama gak Wa ya. Iya ini lagi selesaikan ini soalnya. kan bagus kalo sudah selesai gini gak ada tanggungan jadi bisa tenang. nah sekarang ayo usaha sama-sama. siapa tau hasil dan berkah --Amiin-- <br><br>Jadi kangen rasanya (:V njir Alay). tapi beneran ya. kata guruku puncak kangen itu adalah ketika sepasang pria dan wanita tidak pernah saling bertemu, tidak pernah sms, wa atau email, Tapi ketika malam mereka saling bertemu dalam doa.<br><br>Nah so sweet gak itu.<br><br> Kalo adek sugar sudah nemuin kata-kata ini langsung aja Foto terus kirim ke wa nanti tak tukar sama hadiah untuk adek :D."];
                $msg_main = array("status"=>true, "msg"=>"Good you alright.. :), Next Question ya..");
            }else{
                // $def_return_arr = ["title_inp"=>"Who you are ..? *special name", "def_num"=>"13", "data"=>""];
                $msg_main = array("status"=>false, "msg"=>"Wrong answer.. :D try again Back to first Question.");
            }
        }else{
            $msg_detail["inp"]= strip_tags(form_error('inp'));
        }

        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        $res_msg["item"] = $def_return_arr;
        print_r(json_encode($res_msg));
    }
}
