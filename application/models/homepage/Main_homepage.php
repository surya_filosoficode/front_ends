<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Main_homepage extends CI_Model{

    // Product Main
    public function get_product_for_home_page_all($where, $per_page, $limit){
        $this->db->join("toko tk", "pr.id_toko = tk.id_toko");
        // $this->db->join("product_brand pb", "pr.id_brand = pb.id_brand");
        // $this->db->like('pr.category_produk', $param1);
        $this->db->order_by("id_product", "DESC");

        $data = $this->db->get_where("product pr", $where, $per_page, $limit);

        return $data->result();
    }

    // Product Main Limit
    public function get_product_for_home_page($where, $param1, $per_page, $limit){
        $this->db->join("toko tk", "pr.id_toko = tk.id_toko");
        // $this->db->join("product_brand pb", "pr.id_brand = pb.id_brand");
        $this->db->like('pr.category_produk', $param1);
        $this->db->order_by("id_product", "DESC");
        $data = $this->db->get_where("product pr", $where, $per_page, $limit);

        return $data->result();
    }
}
?>