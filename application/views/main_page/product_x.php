    <?php include 'template/header_menu.php'; ?>

    <!-- ***** Breadcumb Area Start ***** -->
    <!-- <div class="fancy-breadcumb-area bg-img bg-overlay" style="background-image: url(<?php print_r(base_url());?>assets/template/img/bg-img/hero-1.jpg);">
        <div class="container h-100">
            <div class="row h-100 align-items-center">
                <div class="col-12">
                    <div class="breadcumb-content text-center">
                        <h2>Produk</h2>
                    </div>
                </div>
            </div>
        </div>
    </div> -->
    <!-- ***** Breadcumb Area End ***** -->

    <!-- ***** Breadcumb Area Start ***** -->
    <!-- <div class="fancy-breadcumb-area bg-img bg-overlay" style="background-image: url(<?php print_r(base_url());?>assets/template/img/bg-img/hero-1.jpg);"> -->
    <div class=".fancy-breadcumb-area bg-img-blog bg-overlay">
        <div class="container h-100">
            <div class="row h-100 align-items-center">
                <div class="col-12">
                    <div class="breadcumb-content text-center">
                        <!-- <h2>SUPER CASHBACK - Filosofi_code bagi-bagi angpau Lebaran</h2> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ***** Breadcumb Area End ***** -->

    <div class="post-header text-center">
        <?=$str_header?>
    </div>

    <!-- ****** Single Blog Area Start ****** -->
    <section class="single_blog_area section-padding-100">
        <div class="container" style="max-width: 90%;">
            <div class="row">
                <!-- ****** Blog Sidebar ****** -->
                <div class="col-10 col-lg-2">
                    <div class="blog-sidebar">
                        <!-- Single Widget Area -->
                        <div class="single-widget-area popular-post-widget" style="margin-top: 70px;">
                            <div class="widget-title">
                                <h5>Category</h5>
                            </div>
                            <div class="single-populer-post">
                                <div class="post-contents">
                                    <a href="<?=base_url();?>page/product?cate=all">
                                        <h6>Tampilkan Semua</h6>
                                    </a>
                                </div>
                            </div>

                            <?php
                                if(isset($list_pr_category)){
                                    if($list_pr_category){
                                        foreach ($list_pr_category as $key => $value) {
                                            // print_r($value);
                                            $id_jenis        = $value->id_jenis;
                                            $align_header_pr = $value->align_header_pr;
                                            $r_num_pr        = $value->r_num_pr;

                                            $tgl_add_pr = $value->tgl_add_pr;
                                            $add_by_pr  = $value->add_by_pr;

                                            $sts_pr     = $value->sts_pr;
                                            $active_pr  = $value->active_pr;

                                            $nama_jenis = $value->nama_jenis;
                                            $parent_id  = $value->parent_id;
                            ?>
                            <div class="single-populer-post">
                                <div class="post-contents">
                                    <a href="<?=base_url();?>page/product?cate=<?=hash("sha256", $id_jenis)?>">
                                        <h6><?=$nama_jenis?></h6>
                                    </a>
                                </div>
                            </div>
                            <?php
                                        }
                                    }
                                }
                            ?>
                            
                        </div>
                        
                        
                        <!-- Single Widget Area -->
                        <div class="single-widget-area tags-widget mt-5">
                            <div class="widget-title">
                                <h5>Popular Tags</h5>
                            </div>
                            <a href="#">Website</a>
                            <a href="#">Android</a>
                            <a href="#">Photography</a>
                            <a href="#">Desain</a>
                            <a href="#">Wordpress Template</a>
                            <a href="#">Quotes</a>
                            <a href="#">Article</a>
                        </div>
                    </div>
                </div>
                <!-- ****** Blog Sidebar ****** -->

                <div class="col-12 col-lg-10">
                    <div class="row">
                        
                        <div class="container">
                            <div class="row">
                                
                                <div class="col-md-12" style="margin-top: 20px;">
                                    <div class="row">
                        <?php
                            if(isset($list_pr_product)){
                                if($list_pr_product){
                                        // $product_list = $value["product"];
                                    foreach ($list_pr_product as $keyp => $valuep) {
                                        $id_product     = $valuep->id_product;
                                        $nama_product   = $valuep->nama_product;
                                        $id_toko        = $valuep->id_toko;
                                        $id_brand       = $valuep->id_brand;
                                        $category_produk = $valuep->category_produk;
                                        $desc_product   = $valuep->desc_product;
                                        $spec_product   = $valuep->spec_product;
                                        $img_list_product = $valuep->img_list_product;
                                        $tag_product    = $valuep->tag_product;
                                        $price_product  = $valuep->price_product;
                                        $disc_product   = $valuep->disc_product;
                                        $sts_pr_product = $valuep->sts_pr_product;
                                        $sts_nego_product = $valuep->sts_nego_product;

                                        $tipe_owner = $valuep->tipe_owner;
                                        $id_owner = $valuep->id_owner;
                                        $nama_toko = $valuep->nama_toko;
                                        $desc_toko = $valuep->desc_toko;
                                        $main_img_toko = $valuep->main_img_toko;


                                        $img_list_product = str_replace("base_url/", $base_url, $img_list_product);

                                        $main_img_toko = str_replace("base_url/", $base_url, $main_img_toko);

                                        $page_img = "";
                                        $one_page = json_decode($img_list_product);
                                        if($one_page){
                                            $page_img = $one_page[0];
                                        }

                                        // sts_negosiate
                                            $str_sts_nego = "<span class=\"label label-orange\">Nego</span>&nbsp;&nbsp;";
                                            if($sts_nego_product == "0"){
                                                $str_sts_nego = "";
                                            }

                                        // price
                                            $t_pr_prd = $price_product - $disc_product;

                                            $procentage = 0;
                                            if($disc_product != 0 && $price_product != 0){
                                                $procentage = (float)$disc_product/(float)$price_product*100;
                                            }

                                        // sts_negosiate
                                            $str_pr_product = "<p class=\"main_content_product\">".$str_sts_nego."<a href=\"".base_url()."page/contact\">Harga Nego</a></p>";
                                            if($sts_pr_product == "0"){
                                                $str_pr_product = "<p class=\"main_content_product\">".$str_sts_nego."Rp. ".number_format($price_product, 0, ",", ".")."</p>";

                                                if($disc_product != 0){
                                                    $str_pr_product = "<p class=\"main_content_product\">".$str_sts_nego."Rp. ".number_format($t_pr_prd, 0, ",", ".")."</p>";
                                                }
                                            }
                        ?>
                                        <div class="col-12 col-md-4">
                                            <div class="single-blog-area wow fadeInUp">
                                                <img src="<?=$page_img?>" alt="">
                                                <div class="blog-content">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <h5><a href="static-page.html"><?=$nama_product?></a></h5>
                                                        </div>
                                                        <div class="col-md-8">
                                                            <!-- <p class="main_content_product">Rp. <?=number_format($price_product, 0, ',' , '.')?></p> -->
                                                            <?=$str_pr_product?>
                                                        </div>
                                                        <div class="col-md-4 text-right">
                                                            <a href="<?=base_url()."page/product_detail?prd=".hash("sha256", $id_product);?>"><h4><i class="ti-layout-grid2-alt"></i></h4></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    
                        <?php
                                    }
                                }
                            }

                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>                    
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ****** Single Blog Area End ****** -->

    <?php include 'template/footer_menu.php';?>