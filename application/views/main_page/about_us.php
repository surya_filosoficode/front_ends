    <?php
        include 'template/header_menu.php';
    ?>

    <!-- ***** Breadcumb Area Start ***** -->
    <!-- <div class="fancy-breadcumb-area bg-img bg-overlay" style="background-image: url(<?php print_r(base_url());?>assets/template/img/bg-img/hero-1.jpg);"> -->
    <div class=".fancy-breadcumb-area bg-img-blog bg-overlay">
        <div class="container h-100">
            <div class="row h-100 align-items-center">
                <div class="col-12">
                    <div class="breadcumb-content text-center">
                        <!-- <h2>About Us</h2> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ***** Breadcumb Area End ***** -->

    <div class="post-header text-center">
        Narasi Singkat
    </div>

    <!-- ***** About Us Area Start ***** -->
    <section class="fancy-about-us-area section-padding-100" style="padding-top: 100px;">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-12">
                    <!-- <div class="section-heading heading-black text-left">
                        <h2>Narasi Singkat</h2>
                    </div> -->
                    <div class="section-heading heading-black text-left" align="justify">
                        <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Kami hadir untuk anda para daster lovers yg suka dengan model2 daster katun yang cantik dan menarik dengan bordir aplikasi yg unik. Dirintis oleh Ibu Endang Widati, SH pada tahun 1998 dari satu pesanan hingga ratusan pesanan, dan akhirnya membuat offline store di Jl. Raya Wendit Barat, Perum Bumi Mangliawan Kav 12, Malang mulai tahun 2008 sampai sekarang. End's Collection jual daster aja nih? Tentu tidak! Selain daster, longdress, baby doll, dan piama berbahan  katun jepang yg adem, kami juga menyediakan gamis unt.acara formal  dan tunik untuk bekerja atau bepergian santai. Selain itu, juga ada berbagai macam handycraft, mukenah, souvenir, hingga sandal yang dibordir cantik dan trendi. Tersedia juga daster lucu dan cantik untuk anak balita.</p>
                        <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Semuanya dikerjakan sendiri di home industry kami oleh para penjahit2 handal dan kompeten di bidangnya. Info lebih lanjut tentang pemesanan partai atau lebih dari 10 bisa menghubungi contact person yg tertera. Kami juga melayani custom ukuran dan pemesanan masker kain lusinan. </p>
                        <p>So, just take a look on our catalog and happy shopping.</p>
                    </div>
                </div>
            </div>
            

            
        </div>
    </section>
    <!-- ***** About Us Area End ***** -->

   
   

    

    <?php include 'template/footer_menu.php';?>
