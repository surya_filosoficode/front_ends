<!-- ****** Single Blog Area Start ****** -->
    <section class="fancy-about-us-area bg-gray section-padding-100">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-12">
                    <div class="section-heading heading-black text-left">
                        <h2>Title</h2>
                    </div>
                    <div class="section-heading heading-black text-left" align="justify">
                        <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Content</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ***** About Us Area End ***** -->

    <!-- ***** Service Area Start ***** -->
    <section class="fancy-about-us-area">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-12">
                    <div class="section-heading heading-black text-center">
                        <h2>Title</h2>
                        <p>Content</p>
                    </div>
                </div>
            </div>

            <div class="row">
                <!-- Single Service -->
                <div class="col-12 col-md-4">
                    <div class="single-service-area text-center wow fadeInUp" data-wow-delay="0.5s">
                        <i class="ti-desktop"></i>
                        <h2>Title</h2>
                        <p>Content</p>
                    </div>
                </div>
                <!-- Single Service -->
                <div class="col-12 col-md-4">
                    <div class="single-service-area text-center wow fadeInUp" data-wow-delay="0.5s">
                        <i class="ti-ruler-pencil"></i>
                        <h2>Title</h2>
                        <p>Content</p>
                    </div>
                </div>
                <!-- Single Service -->
                <div class="col-12 col-md-4">
                    <div class="single-service-area text-center wow fadeInUp" data-wow-delay="0.5s">
                        <i class="ti-android"></i>
                        <h2>Title</h2>
                        <p>Content</p>
                    </div>
                </div>                
            </div>
        </div>
    </section>
    <!-- ***** Service Area End ***** -->

    <!-- ***** Skills Area Start ***** -->
    <section class="fancy-skills-area bg-gray section-padding-100">
        <div class="about-us-text" align="justify">
            <p>
                <img class="skills-side-thumb-img" src="http://localhost:8080/adm_filcod/assets/img/all_img/img_20200417111835_0.png" style="float: left;">
                <span style="background-color: rgb(247, 247, 247); color: rgb(43, 45, 49); font-size: 14px; text-align: justify;">
                    <div class="section-heading">
                        <h2>Title</h2>
                    </div>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Filosofi_code adalah perusahaan di bidang software dan desain developer yang berpusat di kota Malang, Indonesia. Filosofi_code didirikan pada tanggal 1 Mei 2015. Semagat kami mendirikan perusahaan ini ialah untuk mejadikan Tehnologi dapat digunakan siapapun yang membutuhkan dengan mudah, cepat dan tepat hasilnya untuk membantu kegiatan manusia baik pribadi, kelompok, atau perusahaan. Tentu saja dengan biaya yang sesuai dengan apa yang akan didapatkan dari produk-produk kami. Dengan komitmen penuh dalam mengembangkan aplikasi kami ingin menghadirkan aplikasi yang sesuai kebutuhan anda, dan membantu hidup anda menjadi lebih mudah serta menyenangkan. Tentu dengan berdasar pada komitmen kami antara lain..
                </span>
            </p>
        </div>
        <!-- Side Thumb -->
        <!-- <div class="skills-side-thumb">
            <img class=".img-article" src="http://localhost:8080/adm_filcod/assets/img/all_img/img_20200417111835_0.png" alt="">
            <div class="section-heading">
                <h2>Title</h2>
            </div>
            <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Filosofi_code adalah perusahaan di bidang software dan desain developer yang berpusat di kota Malang, Indonesia. Filosofi_code didirikan pada tanggal 1 Mei 2015. Semagat kami mendirikan perusahaan ini ialah untuk mejadikan Tehnologi dapat digunakan siapapun yang membutuhkan dengan mudah, cepat dan tepat hasilnya untuk membantu kegiatan manusia baik pribadi, kelompok, atau perusahaan. Tentu saja dengan biaya yang sesuai dengan apa yang akan didapatkan dari produk-produk kami. Dengan komitmen penuh dalam mengembangkan aplikasi kami ingin menghadirkan aplikasi yang sesuai kebutuhan anda, dan membantu hidup anda menjadi lebih mudah serta menyenangkan. Tentu dengan berdasar pada komitmen kami antara lain..</p>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-6 col-xl-5 ml-auto">
                    <div class="section-heading">
                        <h2>Title</h2>
                    </div>

                    <div class="about-us-text" align="justify">
                        <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Filosofi_code adalah perusahaan di bidang software dan desain developer yang berpusat di kota Malang, Indonesia. Filosofi_code didirikan pada tanggal 1 Mei 2015. Semagat kami mendirikan perusahaan ini ialah untuk mejadikan Tehnologi dapat digunakan siapapun yang membutuhkan dengan mudah, cepat dan tepat hasilnya untuk membantu kegiatan manusia baik pribadi, kelompok, atau perusahaan. Tentu saja dengan biaya yang sesuai dengan apa yang akan didapatkan dari produk-produk kami. Dengan komitmen penuh dalam mengembangkan aplikasi kami ingin menghadirkan aplikasi yang sesuai kebutuhan anda, dan membantu hidup anda menjadi lebih mudah serta menyenangkan. Tentu dengan berdasar pada komitmen kami antara lain..</p>
                    </div>                    
                </div>
            </div>
        </div> -->
    </section>
    <!-- ***** Skills Area End ***** -->

    <!-- ***** About Us Area Start ***** -->
    <section class="fancy-about-us-area">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-6">
                    <div class="section-heading">
                        <h2>Title</h2>
                        <p></p>
                    </div>

                    <div class="about-us-text" align="justify">
                        <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Content</p>
                    </div>  
                </div>

                <div class="col-12 col-lg-6 col-xl-5 ml-xl-auto">
                    <div class="about-us-thumb wow fadeInUp" data-wow-delay="0.5s">
                        <img src="http://localhost:8080/filosoficode/assets/img/other/happy_work.jpg" alt="">
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ***** About Us Area End ***** -->

    <!-- ***** Skills Area Start ***** -->
    <section class="fancy-skills-area bg-gray section-padding-100">

        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-6">
                    <div class="about-us-thumb wow fadeInUp" data-wow-delay="0.5s">
                        <img src="http://localhost:8080/filosoficode/assets/img/other/our_success.png" alt="">
                    </div>
                </div>

                <div class="col-12 col-lg-6 col-xl-5 ml-xl-auto">
                    <div class="section-heading">
                        <h2>Title</h2>
                        <p></p>
                    </div>

                    <div class="about-us-text" align="justify">
                        <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Content</p>
                    </div>  
                </div>
            </div>
        </div>
        <!-- Side Thumb -->
        <!-- <div class="skills-side-thumb">
            <img src="http://localhost:8080/filosoficode/assets/img/other/our_success.png" alt="">
        </div> -->
        <!-- Skills Content -->
    </section>
    <!-- ****** Single Blog Area End ****** -->    