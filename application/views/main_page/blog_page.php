    <?php include 'template/header_menu.php'; ?>

    <!-- ***** Breadcumb Area Start ***** -->
    <div class=".fancy-breadcumb-area bg-img-blog bg-overlay">
        <div class="container h-100">
            <div class="row h-100 align-items-center">
                <div class="col-12">
                    <div class="breadcumb-content text-center">
                        <!-- <h2>SUPER CASHBACK - Filosofi_code bagi-bagi angpau Lebaran</h2> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ***** Breadcumb Area End ***** -->

    <!-- ****** Single Blog Area Start ****** -->
    <section class="single_blog_area section-padding-100">
        <div class="container" style="max-width: 90%;">
            <div class="row">
                <!-- ****** Blog Sidebar ****** -->
                <div class="col-10 col-lg-3">
                    <div class="card">
                        
                        <div class="blog-sidebar">
                            <!-- Single Widget Area -->
                            <div class="single-widget-area popular-post-widget" style="margin-top: 0px;">
                                <div class="card-header">
                                    <div class="widget-title">
                                        <h5>Category</h5>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <?php
                                        if(isset($article_tipe)){
                                            if($article_tipe){
                                                foreach ($article_tipe as $key => $value) {
                                                    $id_art_tipe   = $value->id_art_tipe;
                                                    $nama_art_tipe = $value->nama_art_tipe;
                                    ?>
                                    <div class="single-populer-post">
                                        <div class="post-contents">
                                            <a href="<?=base_url();?>page/list_blog?id_ses=<?=$nama_art_tipe?>">
                                                <h6><?=$nama_art_tipe?></h6>
                                            </a>
                                        </div>
                                    </div>
                                    <?php
                                                }
                                            }
                                        }
                                    ?>
                                </div>
                            </div>
                            
                            
                            <!-- Single Widget Area -->
                            <div class="single-widget-area tags-widget mt-5" style="padding-left: 1.25rem ; padding-right: 1.25rem ; padding-bottom: 20px;">
                                <div class="widget-title">
                                    <h5>Popular Tags</h5>
                                </div>
                                <a href="#">Website</a>
                                <a href="#">Android</a>
                                <a href="#">Photography</a>
                                <a href="#">Desain</a>
                                <a href="#">Wordpress Template</a>
                                <a href="#">Quotes</a>
                                <a href="#">Article</a>
                            </div>
                        </div>

                    </div>
                </div>
                <!-- ****** Blog Sidebar ****** -->


                <div class="col-12 col-lg-9">
                    <div class="container">
                        <!-- <div class="card">
                            <div class="card-body"> -->

                        <?php
                        
                            $id_article_main = "";
                            $title_article = "";
                            $category_article = "";
                            $tag_article = "";
                            $main_img_article = "";
                            $content_article = "";

                            $create_admin_article = "";
                            $create_date_article = "";
                            $status_check_article = "";
                            $admin_check_article = "";
                            $date_check_article = "";
                            $status_post_article = "";
                            $admin_post_article = "";
                            $date_post_article = "";
                            $is_delete_article = "";
                            $id_admin = "";
                            $id_tipe_admin = "";
                            $nama_admin = "";

                            $arr_bln = $this->time_master->set_indo_month();

                            if(isset($list_article)){
                                if($list_article){
                                    foreach ($list_article as $key => $value) {
                                        $id_article_main =  hash("sha256", $value->id_article_main);
                                        $title_article = $value->title_article;
                                        $category_article = $value->category_article;
                                        $tipe_article = $value->tipe_article;
                                        $tag_article = $value->tag_article;
                                        $main_img_article = $value->main_img_article;
                                        // $content_article = $value->content_article;

                                        $create_admin_article = $value->create_admin_article;
                                        $create_date_article = $value->create_date_article;
                                        $status_check_article = $value->status_check_article;
                                        $admin_check_article = $value->admin_check_article;
                                        $date_check_article = $value->date_check_article;
                                        $status_post_article = $value->status_post_article;
                                        $admin_post_article = $value->admin_post_article;
                                        $date_post_article = $value->date_post_article;
                                        $is_delete_article = $value->is_delete_article;
                                        $id_admin = $value->id_admin;
                                        $id_tipe_admin = $value->id_tipe_admin;
                                        $nama_admin = $value->nama_admin;

                                        $main_img_article = str_replace("base_url/", $base_url, $main_img_article);

                                        $ex_date = explode(" ", $create_date_article);
                                            $th = explode("-", $ex_date[0])[0];
                                            $m = explode("-", $ex_date[0])[1];
                                            $d = explode("-", $ex_date[0])[2];

                                        $ex_time = explode(":", $ex_date[1]);
                                            $h = $ex_time[0];
                                            $mn = $ex_time[1];

                                        $str_date = $d." ".$arr_bln[(int)$m]." ".$th." ".$h.".".$mn;

                                        $basic_content = str_replace("base_url/", $base_url, $value->content_article);

                                        $content_article = str_replace('  ', '', preg_replace('/[\n]+|[\t]+/', '', strip_tags($value->content_article)));
                                        $str_content_article = $content_article;
                                        if(strlen($str_content_article) >= 300){
                                            $str_content_article = substr($content_article, 0, 300);
                                        }

                                        $set_img_first = preg_match('/< *img[^>]*src *= *["\']?([^"\']*)/i',$basic_content ,$matches);
                                        if($matches){
                                            $set_img_first = $matches[0]."\"/>";
                                        }
                                        // preg_match('/<*img[^>]*src *= *["\']?([^"\']*)/i', $main_img_article, $matches);

                                        // print_r($set_img_first);

                                        $link_dt_art = base_url()."page/blog?id_ses=".$id_article_main;

                        ?>
                            <div class="col-12 col-md-12">
                                <!-- <div class="col-12 col-md-12"> -->
                                <div class="single-blog-area wow fadeInUp" style="visibility: visible; animation-name: fadeInUp;">
                                    <div class="row">
                                        <div class="col-4 col-md-4">
                                            <?=$set_img_first?>
                                        </div>
                                        <div class="col-4 col-md-8">
                                            <div class="blog-content" style="padding-top: 10px; padding-left: 10px;padding-right: 15px;min-height: 190px;">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <h5><a href="<?=$link_dt_art?>" style="color: #000000;"><?=$title_article?></a></h5>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <!-- <p class="main_content_product"><span class="label label-orange">Nego</span>&nbsp;&nbsp;Rp. 20.000.000 </p> -->
                                                        <div class="single-top-feature" style="padding-left: 0px;padding-right: 0px;">
                                                            <h6 class="mb-4" style="font-size: 14px;">“<?=$str_content_article?> <i><a class="link-single-top-feature" href="<?=$link_dt_art?>">Read More</a></i>”</h6>
                                                        </div>                                     
                                                    </div>
                                                    <div class="col-md-7 text-right">
                                                        <p class="main_content_product" style="margin-bottom: 5px; font-size: 14px;"><span class="label label-info"><?=$tipe_article?></span>&nbsp;&nbsp; <?= $str_date?></p>
                                                    </div>
                                                    <div class="col-md-5 text-right">
                                                        <a href="#" style="color: #000000;margin-bottom: 5px; font-size: 14px;">By. <?= $nama_admin?></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- </div> -->
                            </div>
                        <?php
                                    }
                                }
                            }
                        ?>
                            
                            <!-- </div>
                        </div> -->
                    </div>
                </div>
                
            </div>
        </div>
    </section>
    <!-- ****** Single Blog Area End ****** -->
    <?php include 'template/footer_menu.php';?>