    <!-- ***** Footer Area Start ***** -->
    <footer class="fancy-footer-area fancy-bg-dark">
        <div class="footer-content section-padding-80-50">
            <div class="container">
                <div class="row">
                    <!-- Footer Widget -->
                    <div class="col-12 col-sm-6 col-lg-4">
                        <div class="single-footer-widget">
                            <h6>Berita dari Kami</h6>
                            <p>Subscribe akun kami, untuk mendapatkan info dan promo terbaru dari kami.</p>
                            <form action="#" method="get">
                                <input type="search" name="search" id="footer-search" placeholder="E-mail">
                                <button type="submit">Subscribe</button>
                            </form>
                            <!-- <div class="footer-social-widegt d-flex align-items-center"> -->
                                <!-- <a href="https://www.facebook.com/yukpesenaplikasibagus/"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                                <a href="https://www.instagram.com/filosoficode/"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                                <a href="#">&nbsp;</a>
                                <a href="#">&nbsp;</a>
                                <a href="#">&nbsp;</a> -->
                                <!-- <a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a> -->
                                <!-- <a href="#"><i class="fa fa-pinterest" aria-hidden="true"></i></a> -->
                            <!-- </div> -->
                            <div class="follow-us-area">
                                <h6>Follow Us</h6>
                                <a href="javascript:window.open('https://www.facebook.com/yukpesenaplikasibagus/')" class="facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                                <!-- <a href="https://www.facebook.com/yukpesenaplikasibagus/" class="whatsapp"><i class="fa fa-whatsapp" aria-hidden="true"></i></a> -->
                                <!-- <a href="#" class="twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a> -->
                                <!-- <a href="#" class="google-plus"><i class="fa fa-google-plus" aria-hidden="true"></i></a> -->
                                <a href="javascript:window.open('https://www.instagram.com/filosoficode/')" class="instagram"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                            </div>
                        </div>
                    </div>

                    <!-- Footer Widget -->
                    <div class="col-12 col-sm-6 col-lg-4">
                        <div class="single-footer-widget">
                            <p>&nbsp;</p>
                        </div>
                    </div>

                    <!-- Footer Widget -->
                    <div class="col-12 col-sm-6 col-lg-4">
                        <div class="single-footer-widget">
                            <h6>Contact Us</h6>
                            <p>info@filosoficode.com</p>
                            <p>Perumahan Bumi Mangliawan, Kavling 12, Desa Mangliawan, Kecamatan Pakis<br>Kabupaten Malang, Indonesia</p>
                            <p>Buka : Senin-Minggu, pukul 8.00 - 16.00</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Footer Copywrite -->
        <div class="footer-copywrite-area">
            <div class="container h-100">
                <div class="row h-100">
                    <div class="col-12 h-100">
                        <div class="copywrite-content h-100 d-flex align-items-center justify-content-between">
                            <!-- Copywrite Text -->
                            <div class="copywrite-text">
                                <p><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                                    Copyright &copy;<script>document.write(new Date().getFullYear());</script> template by <a href="https://colorlib.com" target="_blank">Colorlib</a> & original content by <a href="https://filosoficode.com">filosofi_code</a> for someone special <a href="javascript:void(0);" onclick="go()">-> shmily <-</a><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- ***** Footer Area End ***** -->

    <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" id="for_you" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="exampleModalLabel1">Who am I and Who you are?</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <!-- <form action="<?= base_url()."admin_super/superadmin/update_admin";?>" method="post"> -->
                <div class="modal-body">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="message-text" class="control-label" id="label_inp">Tell me who am i, my nickname is..? <span style="color: red;">*</span></label>
                            <input type="text" class="form-control" id="inp" name="inp" required="">
                            <p id="msg_inp" style="color: red;"></p>
                        </div>
                    </div>         
                </div>
                <div class="modal-footer">
                    <button type="submit" id="send" class="btn waves-effect waves-light btn-rounded btn-info">Send</button>
                    <button type="button" id="refresh" class="btn waves-effect waves-light btn-rounded btn-danger">Refresh</button>
                </div>
                <!-- </form> -->
            </div>
        </div>
    </div>

    <!-- jQuery-2.2.4 js -->
    <script src="<?php print_r(base_url());?>assets/template/js/jquery/jquery-2.2.4.min.js"></script>
    <!-- Popper js -->
    <script src="<?php print_r(base_url());?>assets/template/js/bootstrap/popper.min.js"></script>
    <!-- Bootstrap-4 js -->
    <script src="<?php print_r(base_url());?>assets/template/js/bootstrap/bootstrap.min.js"></script>
    <!-- All Plugins js -->
    <script src="<?php print_r(base_url());?>assets/template/js/others/plugins.js"></script>
    <!-- Active JS -->
    <script src="<?php print_r(base_url());?>assets/template/js/active.js"></script>


    <!-- <script src="<?php print_r(base_url());?>assets/js/jquery-3.2.1.js"></script> -->
    <script type="text/javascript">
        var def_num = 19;

        $("#refresh").click(function(){
            window.location.href = "<?=base_url();?>";
        });

        function go() {
            $("#for_you").modal('show');
        }

        $("#send").click(function(){
            var data_main = new FormData();
            data_main.append('inp', $("#inp").val());
            data_main.append('def_num', def_num);
            

            $.ajax({
                url: "<?php echo base_url()."just_for_you";?>",
                dataType: 'html',
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    response_call(res);
                }
            });
        });

        function response_call(res) {
            var data_json = JSON.parse(res);
            var main_msg = data_json.msg_main;
            var detail_msg = data_json.msg_detail;
            var item = data_json.item;
            if (main_msg.status) {
                var title_inp = item.title_inp;
                def_num = item.def_num;
                var data = item.data;

                var msg = main_msg.msg;

                $("#label_inp").html(title_inp);
                $("#inp").val("");
                $("#msg_inp").html("");

                if(data != "0"){
                    $("#inp").attr("hidden", true);
                    $("#msg_inp").attr("hidden", true);
                    $("#label_inp").html(data);
                }

                alert(msg);
            } else {
                var title_inp = item.title_inp;
                def_num = item.def_num;
                var data = item.data;

                var msg = main_msg.msg;
                $("#label_inp").html(title_inp);

                $("#msg_inp").html(detail_msg.inp);

                alert(msg);
            }
        }


        function getresult(url) {
            $.ajax({
                url: url,
                type: "GET",
                data: {
                    rowcount: $("#rowcount").val(), "pagination_setting": $("#pagination-setting").val()},
                beforeSend: function() {
                    // $("#overlay").show();
                },
                success: function(data) {
                    $("#pagination-result").html(data);
                    // setInterval(function() {
                    //     $("#overlay").hide();
                    // }, 500);
                },
                error: function() {}
            });
        }

        function changePagination(option) {
            if (option != "") {
                getresult("getresult.php");
            }
        }
    </script>
</body>