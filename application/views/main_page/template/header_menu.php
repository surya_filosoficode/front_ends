<!DOCTYPE html>
<html lang="en">
<?php
    //$base_url = "http://localhost:8080/adm_filcod//";
$base_url = "http://adm.endscollection.com/";
?>

<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- The above 4 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <link rel="icon" type="image/png" sizes="16x16" href="<?php print_r($base_url);?>assets/img/logo.png">
    <!-- Title -->
    <title>Ends Collection - Store</title>
    <!-- Favicon -->
    <link rel="icon" href="<?php print_r(base_url());?>assets/template/img/core-img/favicon.ico">
    <!-- Core Stylesheet -->
    <link href="<?php print_r(base_url());?>assets/template/style.css" rel="stylesheet">
    <!-- Responsive CSS -->
    <link href="<?php print_r(base_url());?>assets/template/css/responsive/responsive.css" rel="stylesheet">
    <!-- <script src="<?php print_r(base_url());?>assets/js/leaflet/leaflet.js"></script> -->

    <!-- Bootstrap core CSS -->

    <!-- <link href="<?php print_r(base_url());?>assets/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet"> -->
    <!-- Custom styles for this template -->
    <link href="<?php print_r(base_url());?>assets/bootstrap/docs/4.0/examples/carousel/carousel.css" rel="stylesheet">

    <style type="text/css">

        .main_content_basic {
          font-size: 13px;
          text-align: justify;
        }

        .main_content_product {
            font-size: 15px;
            text-align: justify;
            color: #5a5a5a;
        }

        .img_product {
            width: 100%;
            height: auto;
        }

        .font_color {
          color: #000000;
        }

        /*body {
            width: 600px;
            font-family: "Helvetica Neue", HelveticaNeue, Helvetica, Arial, sans-serif;
            font-size: 14px;
        }*/

        .link {
            padding: 10px 15px;
            background: transparent;
            border: #bccfd8 1px solid;
            border-left: 0px;
            cursor: pointer;
            color: #607d8b
        }

        .disabled {
            cursor: not-allowed;
            color: #bccfd8;
        }

        .current {
            background: #bccfd8;
        }

        .first {
            border-left: #bccfd8 1px solid;
        }

        .question {
            font-weight: bold;
        }

        .answer {
            padding-top: 10px;
        }

        #pagination {
            margin-top: 20px;
            padding-top: 30px;
            border-top: #F0F0F0 1px solid;
        }

        .dot {
            padding: 10px 15px;
            background: transparent;
            border-right: #bccfd8 1px solid;
        }

        #overlay {
            background-color: rgba(0, 0, 0, 0.6);
            z-index: 999;
            position: absolute;
            left: 0;
            top: 0;
            width: 100%;
            height: 100%;
            display: none;
        }

        #overlay div {
            position: absolute;
            left: 50%;
            top: 50%;
            margin-top: -32px;
            margin-left: -32px;
        }

        .page-content {
            padding: 20px;
            margin: 0 auto;
        }

        .pagination-setting {
            padding: 10px;
            margin: 5px 0px 10px;
            border: #bccfd8 1px solid;
            color: #607d8b;
        }

    </style>

    <script data-ad-client="ca-pub-2413580110003566" async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
</head>



<body>
    <!-- Preloader Start -->
    <div id="preloader">
        <div class="loader">
            <span class="inner1"></span>
            <span class="inner2"></span>
            <span class="inner3"></span>
        </div>
    </div>

    <!-- Search Form Area -->

    <div class="fancy-search-form d-flex align-items-center">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <!-- Close Btn -->
                    <div class="search-close-btn" id="closeBtn">
                        <i class="ti-close" aria-hidden="true"></i>
                    </div>

                    <!-- Form -->
                    <form action="#" method="get">
                        <input type="search" name="fancySearch" id="search" placeholder="| Enter Your Search...">
                        <input type="submit" class="d-none" value="submit">
                    </form>
                </div>
            </div>
        </div>
    </div>



    <!-- ***** Header Area Start ***** -->
    <header class="header_area" id="header">
        <div class="container-fluid h-100">
            <div class="row h-100">
                <div class="col-12 h-100">
                    <nav class="h-100 navbar navbar-expand-lg align-items-center">
                        <a class="navbar-brand" href="<?=base_url()?>">End's Collection</a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#fancyNav" aria-controls="fancyNav" aria-expanded="false" aria-label="Toggle navigation"><span class="ti-menu"></span></button>

                        <div class="collapse navbar-collapse" id="fancyNav">
                            <ul class="navbar-nav ml-auto">
                                <li class="nav-item active">
                                    <a class="nav-link" href="<?php print_r(base_url()."page/home");?>">Home<span class="sr-only">(current)</span></a>
                                </li>

                                <!-- <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Pages</a>
                                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                        <a class="dropdown-item" href="index.html">Home</a>
                                        <a class="dropdown-item" href="static-page.html">Static Page</a>
                                        <a class="dropdown-item" href="contact.html">Contact</a>
                                    </div>
                                </li> -->

                                <li class="nav-item">
                                    <a class="nav-link" href="<?php print_r(base_url()."page/product");?>">Product</a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link" href="<?php print_r(base_url()."page/list_blog");?>">Blog</a>
                                </li>
                                <!-- <li class="nav-item">
                                    <a class="nav-link" href="<?php print_r(base_url()."page/relation");?>">Our Relation</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="<?php print_r(base_url()."page/laboratories");?>">Laboratories</a>
                                </li> -->
                                <li class="nav-item">
                                    <a class="nav-link" href="<?php print_r(base_url()."page/contact");?>">Contact</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="<?php print_r(base_url()."page/about_us");?>">About Us</a>
                                </li>
                            </ul>

                            <!-- Search & Shop Btn Area -->
                            <div class="fancy-search-and-shop-area">
                                <a id="search-btn" href="#"><i class="icon_search" aria-hidden="true"></i></a>
                                <a id="shop-btn" href="#"><i class="icon_bag_alt" aria-hidden="true"></i></a>
                            </div>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </header>
    <!-- ***** Header Area End ***** -->