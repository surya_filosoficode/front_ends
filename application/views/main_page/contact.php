    <?php
        include 'template/header_menu.php';
    ?>

    <!-- ***** Breadcumb Area Start ***** -->
    <!-- <div class="fancy-breadcumb-area bg-img bg-overlay" style="background-image: url(<?php print_r(base_url());?>assets/template/img/bg-img/hero-1.jpg);"> -->
    <div class=".fancy-breadcumb-area bg-img-blog bg-overlay">
        <div class="container h-100">
            <div class="row h-100 align-items-center">
                <div class="col-12">
                    <div class="breadcumb-content text-center">
                        <!-- <h2>Contact Us</h2> -->
                        <!-- <p>Tell us about your story and your project.</p> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ***** Breadcumb Area End ***** -->

    <!-- <div class="post-header text-center">
        Kontak Kami
    </div> -->
    <!-- ***** About Us Area Start ***** -->
    <section class="fancy-about-us-area section-padding-100" style="padding-top: 100px;">
        <!-- <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="col-md-12 text-center">
                                <img src="<?php print_r($base_url);?>assets/img/all_img/img_20200502112141_2.png" class="rounded_img" style="width:200px; height: 200px;">
                            </div>
                            <br>
                            <div class="col-md-12 text-center">
                                <h5><a href="https://wa.me/6281230695774?text=Salam%20Success%20admin,%20mohon%20bantuannya%20untuk%20memilih%20produk%20filosofi_code.com%20yang%20cocok%20untuk%20kami." target="_blink">Surya</a></h5>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="col-md-12 text-center">
                                <img src="<?php print_r($base_url);?>assets/img/all_img/img_20200502112141_4.png" class="rounded_img" style="width:200px; height: 200px;">
                            </div>
                            <br>
                            <div class="col-md-12 text-center">
                                <h5><a href="https://wa.me/6281230695774?text=Salam%20Success%20admin,%20mohon%20bantuannya%20untuk%20memilih%20produk%20filosofi_code.com%20yang%20cocok%20untuk%20kami." target="_blink">Meilinda Ika</a></h5>
                            </div>
                        </div>

                        <div class="col-md-4 text-center">
                            <div class="col-md-12 text-center">
                                <img src="<?php print_r($base_url);?>assets/img/all_img/img_20200502112141_3.png" class="rounded_img" style="width:200px; height: 200px;">
                            </div>
                            <br>
                            <div class="col-md-12 text-center">
                                <h5><a href="https://wa.me/6281230695774?text=Salam%20Success%20admin,%20mohon%20bantuannya%20untuk%20memilih%20produk%20filosofi_code.com%20yang%20cocok%20untuk%20kami." target="_blink">Dovi Yudha</a></h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> -->
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-12">
                    <br><br><br>
                    <hr>
                    <br>
                </div>
                <div class="col-12 col-md-6">
                    <!-- Contact Details -->
                    <div class="contact-details-area">
                        <div class="section-heading">
                            <p>Berikut adalah alamat dan juga kontak <i>official</i> resmi kami: </p>
                        </div>
                        <h6>Email:</h6>
                        <p>info@filosoficode.com</p>
                        <h6>Alamat Kantor:</h6>
                        <p>Perumahan Bumi Mangliawan, Kavling 12, Desa Mangliawan, Kecamatan Pakis<br>Kabupaten Malang, Indonesia</p>
                        <h6>Operasional:</h6>
                        <p>Buka : Senin-Minggu, pukul 8.00 - 16.00</p>
                    </div>
                    <!-- Follow Us -->
                </div>
                <div class="col-12 col-md-6">
                    <!-- Contact Form -->
                    <div class="contact-form-area">
                        <div class="section-heading">
                            <h3>Tinggalkan Pesan Untuk Kami</h3>
                            <!-- <p>Fill out the form below to recieve a free and confidential.</p> -->
                        </div>
                        <div class="contact-form">
                            <form action="#" method="post">
                                <!-- Message Input Area Start -->
                                <div class="contact_input_area">
                                    <div class="row">
                                        <!-- Single Input Area -->
                                        <div class="col-12">
                                            <div class="form-group">
                                                <input type="text" class="form-control" name="name" id="name" placeholder="Name" required>
                                            </div>
                                        </div>
                                        <!-- Single Input Area -->
                                        <div class="col-12">
                                            <div class="form-group">
                                                <input type="email" class="form-control" name="email" id="email" placeholder="Email" required>
                                            </div>
                                        </div>
                                        <!-- Single Input Area -->
                                        <div class="col-12">
                                            <div class="form-group">
                                                <input type="text" class="form-control" name="website" id="website" placeholder="Website" required>
                                            </div>
                                        </div>
                                        <!-- Single Input Area -->
                                        <div class="col-12">
                                            <div class="form-group">
                                                <textarea name="message" class="form-control" id="message" cols="30" rows="10" placeholder="Messages" required></textarea>
                                            </div>
                                        </div>
                                        <!-- Single Input Area -->
                                        <div class="col-12">
                                            <button type="submit" class="btn fancy-btn fancy-dark bg-transparent">Send Message</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ***** About Us Area End ***** -->

    
<!-- https://wa.me/15551234567?text=I'm%20interested%20in%20your%20car%20for%20sale -->


    <?php
        include 'template/footer_menu.php';
    ?>

