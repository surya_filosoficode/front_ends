-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 18, 2020 at 05:31 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.2.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `fc`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id_admin` char(12) NOT NULL,
  `id_tipe_admin` varchar(3) NOT NULL,
  `email` text NOT NULL,
  `username` varchar(15) NOT NULL,
  `password` varchar(256) NOT NULL,
  `status_active` enum('0','1','2') NOT NULL,
  `nama_admin` varchar(100) NOT NULL,
  `nip_admin` varchar(64) NOT NULL,
  `is_delete` enum('0','1') NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id_admin`, `id_tipe_admin`, `email`, `username`, `password`, `status_active`, `nama_admin`, `nip_admin`, `is_delete`) VALUES
('AD2019110001', '0', 'suryahanggara@gmail.com', 'surya', '21232f297a57a5a743894a0e4a801fc3', '1', 'surya', '20190001', '0'),
('AD2019110002', '0', 'sudirman@gmail.com', 'sudirman', '21232f297a57a5a743894a0e4a801fc3', '0', 'sudirman', '7838372', '0'),
('AD2019110003', '0', 'dimas@gmail.comx', 'ara', '636bfa0fb2716ff876f5e33854cc9648', '1', 'ara', 'a', '1'),
('AD2019120001', '1', 'vasa@gmail.com', 'vasa', '21232f297a57a5a743894a0e4a801fc3', '1', 'vasa', 'a', '0'),
('AD2019120002', '0', 'didi@gmail.com', 'didi', '21232f297a57a5a743894a0e4a801fc3', '1', 'didi', 'a', '0'),
('AD2020010001', '0', 'surya', 'surya', 'surya', '1', 'asdsad', '1', '0'),
('AD2020010002', '0', 'suryahanggaraxxx@gmail.com', 'donix', '73acd9a5972130b75066c82595a1fae3', '1', 'asdsad', 'a', '0');

-- --------------------------------------------------------

--
-- Table structure for table `admin_tipe`
--
-- Error reading structure for table fc.admin_tipe: #1932 - Table 'fc.admin_tipe' doesn't exist in engine
-- Error reading data for table fc.admin_tipe: #1064 - You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near 'FROM `fc`.`admin_tipe`' at line 1

-- --------------------------------------------------------

--
-- Table structure for table `category`
--
-- Error reading structure for table fc.category: #1932 - Table 'fc.category' doesn't exist in engine
-- Error reading data for table fc.category: #1064 - You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near 'FROM `fc`.`category`' at line 1

-- --------------------------------------------------------

--
-- Table structure for table `data_img`
--
-- Error reading structure for table fc.data_img: #1932 - Table 'fc.data_img' doesn't exist in engine
-- Error reading data for table fc.data_img: #1064 - You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near 'FROM `fc`.`data_img`' at line 1

-- --------------------------------------------------------

--
-- Table structure for table `jenis_product`
--
-- Error reading structure for table fc.jenis_product: #1932 - Table 'fc.jenis_product' doesn't exist in engine
-- Error reading data for table fc.jenis_product: #1064 - You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near 'FROM `fc`.`jenis_product`' at line 1

-- --------------------------------------------------------

--
-- Table structure for table `main_article`
--
-- Error reading structure for table fc.main_article: #1932 - Table 'fc.main_article' doesn't exist in engine
-- Error reading data for table fc.main_article: #1064 - You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near 'FROM `fc`.`main_article`' at line 1

-- --------------------------------------------------------

--
-- Table structure for table `master_brand`
--
-- Error reading structure for table fc.master_brand: #1932 - Table 'fc.master_brand' doesn't exist in engine
-- Error reading data for table fc.master_brand: #1064 - You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near 'FROM `fc`.`master_brand`' at line 1

-- --------------------------------------------------------

--
-- Table structure for table `master_main_category`
--
-- Error reading structure for table fc.master_main_category: #1932 - Table 'fc.master_main_category' doesn't exist in engine
-- Error reading data for table fc.master_main_category: #1064 - You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near 'FROM `fc`.`master_main_category`' at line 1

-- --------------------------------------------------------

--
-- Table structure for table `master_toko`
--
-- Error reading structure for table fc.master_toko: #1932 - Table 'fc.master_toko' doesn't exist in engine
-- Error reading data for table fc.master_toko: #1064 - You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near 'FROM `fc`.`master_toko`' at line 1

-- --------------------------------------------------------

--
-- Table structure for table `productx`
--
-- Error reading structure for table fc.productx: #1932 - Table 'fc.productx' doesn't exist in engine
-- Error reading data for table fc.productx: #1064 - You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near 'FROM `fc`.`productx`' at line 1

-- --------------------------------------------------------

--
-- Table structure for table `produk`
--
-- Error reading structure for table fc.produk: #1932 - Table 'fc.produk' doesn't exist in engine
-- Error reading data for table fc.produk: #1064 - You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near 'FROM `fc`.`produk`' at line 1

-- --------------------------------------------------------

--
-- Table structure for table `user`
--
-- Error reading structure for table fc.user: #1932 - Table 'fc.user' doesn't exist in engine
-- Error reading data for table fc.user: #1064 - You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near 'FROM `fc`.`user`' at line 1

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id_admin`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
